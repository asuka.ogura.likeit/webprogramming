package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import model.User;

/**
 * ユーザテーブル用のDao
 *
 * 
 */

public class UserDao {

  /**
   * 全てのユーザ情報を取得する
   *
   * @return 全ユーザー情報一覧
   */
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE is_admin = false";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  /**
   * 検索条件に該当する全てのユーザ情報を取得する
   * 
   * @param userId
   * @param userName
   * @param startDate
   * @param endDate
   * @return 検索条件に合うユーザー一覧
   */
  public List<User> search(String userId, String userName, String startDate, String endDate) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();
    int count = 1;
    String keyword;
    ArrayList<String> keyList = new ArrayList<String>();
    


    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "SELECT * FROM user WHERE is_admin = false ";
      
      // StringBuilderを
      StringBuilder stringBuilder = new StringBuilder(sql);


      // 入力値の有無でSQLを追加
      if (!userId.equals("")) {
        stringBuilder.append("AND login_id = ? ");
        keyList.add(userId);
      }


      if (!userName.equals("")) {

        stringBuilder.append("AND name LIKE ? ");
        keyList.add("%" + userName + "%");

      }

      if (!startDate.equals("")) {

        stringBuilder.append("AND birth_date >= ? ");
        keyList.add(startDate);

      }

      if (!endDate.equals("")) {

        stringBuilder.append("AND birth_date <= ? ");
        keyList.add(endDate);

      }



      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(stringBuilder.toString());

      for (Iterator<String> it = keyList.iterator(); it.hasNext();) {
        keyword = it.next();
        pStmt.setString(count, keyword);
        count++;

      }
      

      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  /**
   * ログインIDとパスワードに紐づくユーザ情報を返す
   * 
   * @param loginId
   * @param password
   * @return
   */

  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



  /**
   * DBのIDに紐づくユーザ情報を返す
   * 
   * @param idNum
   * @return 更新するユーザー情報
   */
  public User findById(int idNum) {

    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, idNum);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

  }

  /**
   * ログインIDに紐づくユーザ情報を返す
   * 
   * @param loginId
   * @return
   */

  public User findByLoginId(String loginId) {

    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();


      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

  }

  /**
   * ユーザ情報を登録する
   *
   * @Param loginId
   * @param name
   * @Param password
   * @param birthday
   * @return なし
   */


  public void insertUser(String loginId, String name, String birthDate, String passsword) {

    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // INSERT文を準備
      String sql =
          "INSERT INTO user (login_id, name, birth_date,password,create_date,update_date) VALUES (?, ?, ?, ?,now(),now())";
      // INSERTを実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, name);
      pStmt.setString(3, birthDate);
      pStmt.setString(4, passsword);


      pStmt.executeUpdate();
      pStmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * ユーザ情報を更新する（パスワードを含む）
   * 
   * @param userId
   * @param name
   * @param birthDate
   * @param passsword
   * @return なし
   */
  
  public void updateUserHasPass(String userId, String name, String birthDate, String passsword) {

    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // INSERT文を準備
      String sql =
          "UPDATE user SET name = ?,birth_date=?,password=?,update_date=now() WHERE id = ?;";
      // INSERTを実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, birthDate);
      pStmt.setString(3, passsword);
      pStmt.setString(4, userId);


      pStmt.executeUpdate();
      pStmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * ユーザ情報を更新する（パスワードを含まない）
   * 
   * @param userId
   * @param name
   * @param birthDate
   * @return なし
   */

  public void updateUserNonPass(String userId, String name, String birthDate) {

    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // INSERT文を準備
      String sql = "UPDATE user SET name = ?,birth_date=?,update_date=now() WHERE id = ?;";
      // INSERTを実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, birthDate);
      pStmt.setString(3, userId);


      pStmt.executeUpdate();
      pStmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



  /**
   * ユーザ情報を削除する
   *
   * @Param loginId
   * @return なし
   */
  public void deleteUser(String userId) {

    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // INSERT文を準備
      String sql = "DELETE FROM user WHERE id = ?;";
      // INSERTを実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userId);


      pStmt.executeUpdate();
      pStmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



}


