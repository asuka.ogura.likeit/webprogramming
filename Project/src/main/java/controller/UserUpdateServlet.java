package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;


/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserUpdateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    /**** ログインの有無確認 start ****/
    HttpSession session = request.getSession();
    User loginUser = (User) session.getAttribute("userInfo");

    /* ユーザ情報が取得できない場合 */
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }
    /**** ログインの有無確認 end ****/

    /* ユーザ情報が取得できた場合 */
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの値を取得
    int id = Integer.valueOf(request.getParameter("id"));

    // リクエストパラメータで取得した値を引数に、findByIdメソッドを実行
    UserDao userDao = new UserDao();
    User userDeta = userDao.findById(id);

    // findByIdメソッドの戻り値をリクエストスコープにセット
    request.setAttribute("user", userDeta);

    /* userUpdate.jspへフォワード */
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
    dispatcher.forward(request, response);



  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String userId = request.getParameter("user-id");
    String loginId = request.getParameter("login-id");
    String userName = request.getParameter("user-name");
    String strBirthDate = request.getParameter("birth-date");
    String password = request.getParameter("password");
    String passwordConfirm = request.getParameter("password-confirm");

    // UserDaoクラスをインスタンス化
    UserDao userDao = new UserDao();
    
    //ユーザーインスタンスの生成
    User user = new User();
   
    
    // パスワード更新の有無で分岐
    if (!(password.equals(""))) {

      /* パスワードを含んで更新する場合 */
      // 例外処理
      // Exception：userUpdate.jspへフォワード
      // 1：パスワードとパスワード(確認)の入力内容が異なる場合
      // 2：未入力の項目がある場合。
      // →userName,birthDate,passwordConfirm

      if (!password.equals(passwordConfirm) || userName.equals("") || strBirthDate.equals("")
          || passwordConfirm.equals("")) {

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力した内容は正しくありません");

        // リクエストスコープに入力項目をセット
        user.setLoginId(loginId);
        user.setName(userName);
        

        /*型変換を必要とするため、生年月日入力時のみ　分岐*/
        if(!strBirthDate.equals("")) {
        //型変換処理
          Date dBirthDate = Date.valueOf(strBirthDate);
          // userインスタンスのsetBirthDateへセット
          user.setBirthDate(dBirthDate);
        }
        
        // リクエストスコープへuserインスタンスをセット
        request.setAttribute("user", user);

        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;


      } else {

        // パスワードの暗号化
        PasswordEncorder pEncorder = new PasswordEncorder();
        String encryptPass = pEncorder.encordPassword(password);


        // ユーザー情報の更新
        userDao.updateUserHasPass(userId, userName, strBirthDate, encryptPass);

      }

    } else {
      /* パスワード以外を更新する場合 */
      if (!password.equals(passwordConfirm) || userName.equals("") || strBirthDate.equals("")) {



        // 例外処理
        // Exception：userUpdate.jspへフォワード
        // 1：パスワードとパスワード(確認)の入力内容が異なる場合
        // 2：未入力の項目がある場合。
        // →userName,birthDate

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力した内容は正しくありません");

        // リクエストスコープに入力項目をセット
        user.setLoginId(loginId);
        user.setName(userName);


        /* 型変換を必要とするため、生年月日入力時のみ 分岐 */
        if (!strBirthDate.equals("")) {
          // 型変換処理
          Date dBirthDate = Date.valueOf(strBirthDate);
          // userインスタンスのsetBirthDateへセット
          user.setBirthDate(dBirthDate);
        }

        // リクエストスコープへuserインスタンスをセット
        request.setAttribute("user", user);

        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;


      } else {

        // ユーザー情報の更新
        userDao.updateUserNonPass(userId, userName, strBirthDate);

      }
    }

    // ユーザ一覧のサーブレットにリダイレクト
    response.sendRedirect("UserListServlet");


  }

}
