package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserAddServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    /**** ログインの有無確認 start ****/
    // ログインチェック(ログインセッションがない場合、ログイン画面にリダイレクトさせる)
    // セッション
    HttpSession session = request.getSession();

    // ログインユーザ情報を取得
    User loginUser = (User) session.getAttribute("userInfo");

    /* ユーザ情報が取得できない場合 */
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;

    }
    /**** ログインの有無確認 end ****/

    // 新規登録のjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
    dispatcher.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // 新規登録画面で入力された情報を取得
    String loginId = request.getParameter("user-loginid");
    String userName = request.getParameter("user-name");
    String birthDate = request.getParameter("birth-date");
    String password = request.getParameter("password");
    String passwordConfirm = request.getParameter("password-confirm");

    // 引数のログインIDをfindByLoginIdメソッドへ渡し、実行 // 例外チェック3で活用
    // お手本コードでは、boolean型のメソッドにして、T or Fの形式で判定している。
    UserDao userDao = new UserDao();
    User userDeta = userDao.findByLoginId(loginId);

    // 例外処理→すべてがFaleseを返したときだけ
    // 例外1.すべての値が入力されたか
    // 例外2.パスワードが2つとも同じか
    // 例外3.入力されたログインIDが既に利用されていないか
    if ((emptyCheck(loginId, userName, birthDate, password, passwordConfirm))
        || !((password.equals(passwordConfirm))) || (userDeta != null)) {

      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "入力した内容は正しくありません");
      
      // リクエストスコープに入力項目をセット
      request.setAttribute("userloginId", loginId);
      request.setAttribute("userName", userName);
      request.setAttribute("userbirthDate", birthDate);


      // userAdd.jspフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      return;

    } else {
      /**** 新規登録成功時 ****/

      // パスワードの暗号化
      PasswordEncorder pEncorder = new PasswordEncorder();
      String encryptPass = pEncorder.encordPassword(password);


      // ユーザー情報の登録
      userDao.insertUser(loginId, userName, birthDate, encryptPass);

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");

    }

  }

  /**
   * 新規登録画面の入力された値が全て入力されているかどうか
   *
   * @param loginId
   * @param password
   * @param passwordConfirm
   * @param userName
   * @param birthdate
   * @return
   */
  // 例外チェック1のメソッド
  protected boolean emptyCheck(String loginId, String userName, String birthDate, String password,
      String passwordConfirm) {

    if (loginId.equals("")) {
      return true;

    } else if (userName.equals("")) {
      return true;

    } else if (birthDate.equals("")) {
      return true;

    } else if (password.equals("")) {
      return true;

    } else if (passwordConfirm.equals("")) {
      return true;

    } else {
      return false;
    }

  }

}
