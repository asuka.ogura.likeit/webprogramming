package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      /**** ログインの有無確認 start ****/
      HttpSession session = request.getSession();
      User loginUser = (User) session.getAttribute("userInfo");

      /* ユーザ情報が取得できない場合 */
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;

      }
      /**** ログインの有無確認 end ****/


      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの値を取得
      int id = Integer.valueOf(request.getParameter("id"));

      // リクエストパラメータで取得した値を引数に、findByIdメソッドを実行
      UserDao userDao = new UserDao();
      User userDeta = userDao.findById(id);

      // findByIdメソッドの戻り値をリクエストスコープにセット
      request.setAttribute("user", userDeta);

      // userDetail.jspへフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
      dispatcher.forward(request, response);
      return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
