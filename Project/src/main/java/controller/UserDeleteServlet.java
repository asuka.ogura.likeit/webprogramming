package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      /**** ログインの有無確認 start ****/
      HttpSession session = request.getSession();
      User loginUser = (User) session.getAttribute("userInfo");

      /* ユーザ情報が取得できない場合 */
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      /**** ログインの有無確認 end ****/

      /* ユーザ情報が取得できた場合 */
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの値を取得
      int id = Integer.valueOf(request.getParameter("id"));

      // リクエストパラメータで取得した値を引数に、findByIdメソッドを実行
      UserDao userDao = new UserDao();
      User userDeta = userDao.findById(id);

      // findByIdメソッドの戻り値をリクエストスコープにセット
      request.setAttribute("user", userDeta);

      /* userUpdate.jspへフォワード */
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
      dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      String userId = request.getParameter("user-id");

      // ユーザー情報の更新
      UserDao userDao = new UserDao();
      userDao.deleteUser(userId);

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("UserListServlet");

	}

}
