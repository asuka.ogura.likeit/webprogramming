﻿-- 1.「usermanagement」という名前でデータベースを作成するSQL文を記述
CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

-- 作成したusermanagementデータベースを操作する命令
USE usermanagement;

-- 2.データベース「usermanagement」に「user」というテーブルを作成するSQL文を記述
CREATE TABLE user(

id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
is_admin boolean NOT NULL DEFAULT 0,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL

);

-- 3.テーブル「user」に管理者のデータを追加するSQL文を記述
INSERT INTO user(login_id,name,birth_date,password,is_admin,create_date,update_date) VALUES(

'admin',
'管理者',
'1999-12-09',
'password',
1,
now(),
now()

);
