﻿--使用するDBの指定
use usermanagement;

--SELECT文 田中太郎の場合
SELECT *
FROM user
WHERE is_admin = false
AND login_id = 'user99'
AND name LIKE '%田中太郎%'
AND birth_date BETWEEN '2000/06/01'AND '2000/06/30';

--SELECT文 一般1の場合
SELECT *
FROM user
WHERE is_admin = false
AND login_id = 'user01'
AND name LIKE '%一般1%'
AND birth_date BETWEEN '2001/12/19'AND '2002/06/30';

--SELECT文 管理者の場合→出力されないが正
SELECT *
FROM user
WHERE is_admin = false
AND login_id = 'admin'
AND name LIKE '%管理者%'
AND birth_date BETWEEN '1999/12/01'AND '1999/12/31';
