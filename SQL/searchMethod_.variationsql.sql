--使用するDBの指定
use usermanagement;

--SELECT文 全条件入力
SELECT *
FROM user
WHERE is_admin = false
AND login_id = 'user01'
AND name LIKE '%一般1%'
AND birth_date BETWEEN '2001/12/19'AND '2002/06/30';

--SELECT文 条件：ログインID時
SELECT *
FROM user
WHERE is_admin = false
AND login_id = 'user01';

--SELECT文 条件：ユーザー名
SELECT *
FROM user
WHERE is_admin = false
AND name LIKE '%一般1%';

--SELECT文 条件：誕生日(開始日)
SELECT *
FROM user
WHERE is_admin = false
AND birth_date <= '2001/12/19';

--SELECT文 条件：誕生日(終了日)
SELECT *
FROM user
WHERE is_admin = false
AND birth_date >= '2002/06/30';

--SELECT文 条件：ログインID時とユーザー名
SELECT *
FROM user
WHERE is_admin = false
AND login_id = 'user01'
AND name LIKE '%一般1%';

--SELECT文 条件：ログインID時と誕生日(開始日)
SELECT *
FROM user
WHERE is_admin = false
AND login_id = 'user01'
AND birth_date <= '2001/12/19';

--SELECT文 条件：誕生日(開始日)と誕生日(終了日)　実験
SELECT *
FROM user
WHERE is_admin = false
AND login_id = 'user01'
AND birth_date >= '2000/12/19'
AND birth_date <= '2002/06/30';
